#pragma once

#include <vector>
#include <fstream>
#include <map>

#include "Strategy.hh"

class SystemStationary;

class StrategyOffline : public Strategy
{
public:
	StrategyOffline();

	virtual ~StrategyOffline();

	virtual void reset();

protected:
	double run(taskset ts, double dt);

	double get_rate_scale(taskset ts) const;

	double get_rate_scale(size_t task_id, taskset ts) const;

	double get_rate(taskset ts) const;

	double get_rate(size_t task_id, taskset ts) const;

	double min_remaining_work() const;

	double min_remaining_work(taskset ts) const;

	double min_remaining_time(taskset ts) const;

	size_t get_next_min_work() const;

	double get_subset_time(taskset ts) const;

	double get_work_total(size_t task_id) const;

private:
	std::map<unsigned long long, double> m_subset_time;
};

