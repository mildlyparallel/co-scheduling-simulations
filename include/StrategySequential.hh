#pragma once

#include "StrategyOnline.hh"

class StrategySequential : public StrategyOnline
{
public:
	StrategySequential();

	virtual ~StrategySequential();

	virtual void start();

	virtual const char *name() const;

protected:

private:

};
