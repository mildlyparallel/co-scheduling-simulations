#pragma once

#include "Task.hh"

class ScheduledTask : public Task
{
public:
	ScheduledTask();

	ScheduledTask(const Task &t);

	virtual ~ScheduledTask();

	bool released() const;

	double get_rate(taskset tasks) const;

	void reset();

	double get_rate_last() const;

	double get_rate_avg() const;

	bool complited() const;

	double get_complition_time() const;

	double run(taskset tasks, double time_now, double delta_time);

	double get_work_done() const;

	double get_rate_scale(taskset tasks) const;

protected:

private:
	void push_rate(double rate);

	void advance(double time_at_end, double delta_work);

	double m_work_done = 0;

	bool m_complited = false;

	double m_complition_time = 0;

	bool m_released = false;

	static const constexpr size_t m_rate_history_size_mask = (1 << RATE_LOG_SIZE) - 1;

	std::array<double, m_rate_history_size_mask + 1> m_rates;

	size_t m_rates_pos = 0;

};
