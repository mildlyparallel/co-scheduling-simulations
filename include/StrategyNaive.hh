#pragma once

#include "StrategyOnline.hh"

class StrategyNaive : public StrategyOnline
{
public:
	StrategyNaive();

	virtual ~StrategyNaive();

	virtual void start();

	virtual const char *name() const;

protected:

private:

};
