#!/bin/bash

module load gcc/10

min_slope=0.45
max_slope=0.55

min_slope=0.55
max_slope=0.65

min_slope=0.65
max_slope=0.75

min_slope=0.75
max_slope=0.85

min_slope=0.85
max_slope=0.95

h=$(hostname)

dir="results/slope/${min_slope}-${max_slope}/$h"

cd /home/rk/rswm-simulations

rm -rf "$dir"
mkdir -p "$dir"

./build/rswmsimul -r 200 -o $dir \
	--seed $RANDOM \
	--tasks-min 10 \
	--tasks-max 11 \
	--slope-scale-min $min_slope \
	--slope-scale-max $max_slope | tee $dir/log

cat $dir/results-*.csv > $dir/results.csv
