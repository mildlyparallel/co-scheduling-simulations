#include <cassert>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "SystemStationary.hh"

SystemStationary::SystemStationary()
: System()
{ }

SystemStationary::SystemStationary(size_t n)
: System(n)
{ }

SystemStationary::~SystemStationary()
{  }

void SystemStationary::init_base_rate(double r, double dr)
{
	for (auto &t : m_tasks) {

		auto rate_fn = [r](double) {
			return r;
		};

		t.set_base_rate_fn(rate_fn);

		r += dr;
	}
}

void SystemStationary::init_base_rate(std::function<double (const Task &)> rate_gen_fn)
{
	for (auto &t : m_tasks) {

		double rate = rate_gen_fn(t);

		auto rate_fn = [rate](double) {
			return rate;
		};

		t.set_base_rate_fn(rate_fn);
	}
}

void SystemStationary::init_rate_scale(std::function<double (taskset)> scale_gen_fn)
{
	size_t n = 1 << m_tasks.size();

	for (size_t j = 1; j < n; ++j) {
		taskset ts(j);

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			double scale = scale_gen_fn(ts);

			assert(scale > 0 && scale <= 1);

			auto scale_fn = [scale](double) {
				return scale;
			};

			m_tasks[i].set_rate_scale_fn(ts, scale_fn);
		}
	}
}

void SystemStationary::init_rate_scale(std::function<double (taskset, const Task &)> scale_gen_fn)
{
	size_t n = 1 << m_tasks.size();

	for (size_t j = 1; j < n; ++j) {
		taskset ts(j);

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			double scale = scale_gen_fn(ts, m_tasks[i]);

			set_rate_scale(i, ts, scale);
		}
	}
}

void SystemStationary::init_scale_rnd(std::function<double (size_t)> scale_fn)
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		// std::cerr << std::string(40, '=') << std::endl;
		init_scale_rnd(i, scale_fn);
	}
}

double SystemStationary::find_min_scale(size_t task_id, taskset ts) const
{
	double min_s = 1;
	assert(ts[task_id]);
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (i == task_id)
			continue;

		if (!ts[i])
			continue;

		ts[i] = false;

		double s = m_tasks[task_id].get_rate_scale(ts, 0);
		// std::cout << "--" << ts << " " << s << std::endl;
		if (s < min_s)
			min_s = s;

		ts[i] = true;
	}

	return min_s;
}

void SystemStationary::init_scale_rnd(
	size_t task_id,
	std::function<double(size_t)> scale_fn
) {
	taskset ts;
	ts[task_id] = 1;

	set_rate_scale(task_id, ts, 1);

	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	for (size_t n = 2; n <= m_tasks.size(); ++n) {
		for (size_t j = 0; j < nr_combinations; ++j) {
			taskset ts(j + 1);
			if (ts.count() != n)
				continue;
			if (!ts[task_id])
				continue;

			double min_speed = find_min_scale(task_id, ts);
			// std::cerr << n << " " << ts << " " << min_speed << std::endl;
			double s = scale_fn(task_id) * min_speed;
			set_rate_scale(task_id, ts, s);
		}
	}
}

void SystemStationary::set_rate_scale(size_t task_id, taskset ts, double scale)
{
	auto scale_fn = [scale](double) {
		return scale;
	};

	m_tasks[task_id].set_rate_scale_fn(ts, scale_fn);
}

void SystemStationary::print_row(const std::string &label, std::function<double (const Task &task)> fn) const
{
	std::cout << std::setw(5) << label << " = [";

	bool comma = false;

	for (auto &t : m_tasks) {
		if (comma)
			std::cout << ", ";
		comma = true;

		std::cout << std::setw(4) << fn(t);
	}

	std::cout << "]" << std::endl;
}

void SystemStationary::print_comb(
	const std::string &label,
	std::function<double (const Task &task, taskset)> fn
) const {
	double w = 10;

	size_t nr_comb = 1 << m_tasks.size();

	std::cout << std::setw(5) << label << " | ";
	for (size_t n = 1; n <= m_tasks.size(); ++n) {
		for (size_t j = 1; j < nr_comb; ++j) {
			taskset ts(j);
			if (ts.count() != n)
				continue;

			std::cout << std::setw(w) << j << " ";
		}
	}

	std::cout << "\n";

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		std::cout << std::setw(5) << i << " | ";
		for (size_t n = 1; n <= m_tasks.size(); ++n) {
			for (size_t j = 1; j < nr_comb; ++j) {
				taskset ts(j);
				if (ts.count() != n)
					continue;

				std::cout << std::setw(w) <<  fn(m_tasks[i], ts) << " ";
			}
		}

		std::cout << "\n";
	}

	std::cout << std::setw(5) << "sum" << " | ";
	for (size_t n = 1; n <= m_tasks.size(); ++n) {
		for (size_t j = 1; j < nr_comb; ++j) {
			taskset ts(j);
			if (ts.count() != n)
				continue;

			double sum = 0;
			for (size_t i = 0; i < m_tasks.size(); ++i) {
				if (ts[i])
					sum += fn(m_tasks[i], ts);
			}

			std::cout << std::setw(w) << sum << " ";
		}
	}

	std::cout << std::endl;
}
void SystemStationary::print_val(const std::string &label, std::function<double ()> fn) const
{
	std::cout << std::setw(5) << label << " = " << fn() << std::endl;
}

void SystemStationary::print() const
{
	print_val("size", [&](){ return m_tasks.size(); });

	print_row("work", [](const Task &task) {
			return task.get_work_total();
	});

	print_row("rel", [](const Task &task) {
			return task.get_release_time();
	});

	print_row("base", [](const Task &task) {
			return task.get_base_rate(0);
	});

	if (m_tasks.size() < 5) {
		std::cerr << std::endl;

		print_comb("scale", [](const Task &task, taskset ts) {
			return task.get_rate_scale(ts, 0);
		});

		std::cerr << std::endl;

		print_comb("rate", [](const Task &task, taskset ts) {
			return task.get_rate(ts, 0);
		});
	}
}

void SystemStationary::write(const std::string &outdir) const
{
	assert(!outdir.empty());

	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	std::stringstream outfile;
	outfile << outdir << "/system-" << get_id() << ".csv";

	std::ofstream out(outfile.str());

	out << "work,base";
	for (size_t j = 0; j < nr_combinations; ++j) {
		for (size_t n = 1; n <= m_tasks.size(); ++n) {
			taskset ts(j+1);
			if (ts.count() != n)
				continue;
			out << ",S" << (j+1);
		}
	}
	out << "\n";

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		out << m_tasks[i].get_work_total() << ",";
		out << m_tasks[i].get_base_rate(0);

		for (size_t n = 1; n <= m_tasks.size(); ++n) {
			for (size_t j = 0; j < nr_combinations; ++j) {
				taskset ts(j+1);
				if (ts.count() != n)
					continue;
				out << "," << m_tasks[i].get_rate_scale(ts, 0);
			}
		}
		out << "\n";
	}
}

void SystemStationary::write_by_task(const std::string &outdir) const
{
	assert(!outdir.empty());

	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	std::stringstream outfile;
	outfile << outdir << "/speedup-task-" << get_id() << ".csv";

	std::ofstream out(outfile.str());

	out << "comb,task,size,speedup\n";

	for (size_t j = 0; j < nr_combinations; ++j) {
		taskset ts(j+1);

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			out << j << ",";
			out << i << ",";
			out << ts.count() << ",";
			out << m_tasks[i].get_rate_scale(ts, 0) << "\n";
		}
	}
}

void SystemStationary::write_by_comb(const std::string &outdir) const
{
	assert(!outdir.empty());

	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	std::stringstream outfile;
	outfile << outdir << "/speedup-comb-" << get_id() << ".csv";

	std::ofstream out(outfile.str());

	out << "comb,size,speedup\n";

	for (size_t j = 0; j < nr_combinations; ++j) {
		taskset ts(j+1);

		double s = 0;
		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;
			s += m_tasks[i].get_rate_scale(ts, 0);
		}

		out << j << ",";
		out << ts.count() << ",";
		out << s << "\n";
	}
}
