#include <cassert>
#include <cmath>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "StrategySearch.hh"

StrategySearch::StrategySearch()
{  }

StrategySearch::~StrategySearch()
{  }

const char *StrategySearch::name() const
{
	return m_name.c_str();
}

void StrategySearch::set_acc_fun(AccFun f)
{
	m_acc_fun = f;

	if (m_acc_fun == UCB)
		m_name = "ucb";
	else if (m_acc_fun == PI)
		m_name = "pi";
	else if (m_acc_fun == EI)
		m_name = "ei";
}

void StrategySearch::write_estimate(const std::string &file) const
{
	std::ofstream out(file);

	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	out << "task";
	for (size_t n = 1; n <= m_tasks.size(); ++n) {
		for (size_t j = 0; j < nr_combinations; ++j) {
			taskset ts(j+1);
			if (ts.count() != n)
				continue;

			out << ",(";
			bool comma = false;
			for (size_t i = 0; i < m_tasks.size(); ++i) {
				if (!ts[i])
					continue;

				if (comma)
					out << ";";
				out << i+1;
				comma = 1;
			}
			out << ")";
		}
	}
	out << "\n";

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		out << "T" << i << "_min";
		for (size_t n = 1; n <= m_tasks.size(); ++n) {
			for (size_t j = 0; j < nr_combinations; ++j) {
				if (taskset(j+1).count() != n)
					continue;

				double ci = m_estimate[i][j].scale - m_estimate[i][j].sigma;
				out << "," << ci;
			}
		}
		out << "\n";
		out << "T" << i << "_mean";
		for (size_t n = 1; n <= m_tasks.size(); ++n) {
			for (size_t j = 0; j < nr_combinations; ++j) {
				if (taskset(j+1).count() != n)
					continue;

				out << "," << m_estimate[i][j].scale;
			}
		}
		out << "\n";
		out << "T" << i << "_max";
		for (size_t n = 1; n <= m_tasks.size(); ++n) {
			for (size_t j = 0; j < nr_combinations; ++j) {
				if (taskset(j+1).count() != n)
					continue;

				double ci = m_estimate[i][j].scale + m_estimate[i][j].sigma;
				out << "," << ci;
			}
		}
		out << "\n";
		out << "T" << i << "_real";
		for (size_t n = 1; n <= m_tasks.size(); ++n) {
			for (size_t j = 0; j < nr_combinations; ++j) {
				taskset ts(j+1);
				if (taskset(ts).count() != n)
					continue;

				out << "," << get_rate_scale(i, ts);
			}
		}
		out << "\n";
	}
}

void StrategySearch::init()
{
	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		m_estimate[i].resize(nr_combinations);

		for (size_t j = 0; j < nr_combinations; ++j) {
			taskset ts(j+1);

			if (!ts[i]) {
				m_estimate[i][j].set(0);
				continue;
			}

			m_estimate[i][j].scale = 0.5;
			m_estimate[i][j].scale_min = 0;
			m_estimate[i][j].scale_max = 1;
		}
		m_ideal_rate[i] = 0;
	}
}

void StrategySearch::collect_all_ideal()
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		taskset ts;
		ts[i] = 1;

		run(ts, time_unit);

		m_ideal_rate[i] = m_tasks[i].get_rate_last();
		m_estimate[i][ts.to_ulong() - 1].set(1);

		m_sampled_combinations.push_back(ts);
	}
}

void StrategySearch::run_and_measure(taskset ts)
{
	assert(!any_complited(ts));
	run(ts, time_unit);

	m_last_scale = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		double rate = m_tasks[i].get_rate_last();
		assert(m_ideal_rate[i] > 0);
		assert(m_ideal_rate[i] >= rate);
		double scale = rate / m_ideal_rate[i];

		m_estimate[i][ts.to_ulong() - 1].set(scale);
		m_estimate[i][ts.to_ulong() - 1].set(scale);
		m_sampled_combinations.push_back(ts);

		m_last_scale += scale;
	}

	if (!any_complited(ts)) {
		m_last_comb = ts;
		return;
	}

	// std::cout << "task finished" << std::endl;

	m_last_scale = 0;

	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	for (size_t j = 0; j < nr_combinations; ++j) {
		taskset ts(j+1);

		bool ok = true;
		double scale = 0;

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			if (m_tasks[i].complited()) {
				ok = false;
				break;
			}

			if (!m_estimate[i][j].exact) {
				ok = false;
				break;
			}

			scale += m_estimate[i][j].scale;
		}

		if (!ok)
			continue;

		if (scale <= m_last_scale)
			continue;

		m_last_scale = scale;
		m_last_comb = ts;
	}

	return;
}

double StrategySearch::interploate_liniear(
	double x,
	double x0,
	double x1,
	double y0,
	double y1
) {
	if (x0 > x1) {
		std::swap(x0, x1);
		std::swap(y0, y1);
	}

	assert(x0 <= x);
	assert(x1 >= x);
	assert(x1 > x0);

	double y = y0 + (y1 - y0) * (x - x0) / (x1 - x0);
	return y;
}

void StrategySearch::interpolate_between(
	size_t task_id,
	taskset ts,
	taskset ts_min,
	taskset ts_max
) {
	assert(ts[task_id]);
	assert(ts_min[task_id]);
	assert(ts_max[task_id]);
	assert(ts_min.count() < ts.count());
	assert(ts_max.count() > ts.count());

	double x = ts.count();
	double x0 = ts_min.count();
	double x1 = ts_max.count();
	double y0 = m_estimate[task_id][ts_min.to_ulong() - 1].scale;
	double y1 = m_estimate[task_id][ts_max.to_ulong() - 1].scale;
	assert(y0 >= y1);

	assert(!m_estimate[task_id][ts.to_ulong() - 1].exact);
	assert(m_estimate[task_id][ts_min.to_ulong() - 1].exact);
	assert(m_estimate[task_id][ts_max.to_ulong() - 1].exact);
	assert(is_subset(ts_min, ts));
	assert(is_subset(ts, ts_max));

	double new_interval = y0 - y1;
	auto &est = m_estimate[task_id][ts.to_ulong() - 1];
	double old_interval = est.scale_max - est.scale_min;
	if (new_interval >= old_interval)
		return;

	double y = interploate_liniear(x, x0, x1, y0, y1);

	double delta = std::min(y0 - y, y - y1);
	assert(delta >= 0);

	est.sigma = delta / m_quratile;
	est.scale = y;
	est.scale_min = y1;
	est.scale_max = y0;
}

void StrategySearch::interpolate_combination(size_t task_id, taskset ts)
{
	assert(ts[task_id]);

	if (m_estimate[task_id][ts.to_ulong() - 1].exact)
		return;

	for (auto ts_min : m_sampled_combinations) {
		if (!ts_min[task_id])
			continue;
		if (ts_min.count() >= ts.count())
			continue;
		if (ts_min == ts)
			return;
		if (!is_subset(ts_min, ts))
			continue;

		for (auto ts_max : m_sampled_combinations) {
			if (!ts_max[task_id])
				continue;
			if (ts_max.count() <= ts.count())
				continue;
			if (ts_max == ts)
				return;
			if (!is_subset(ts, ts_max))
				continue;

			interpolate_between(task_id, ts, ts_min, ts_max);
		}
	}
}

void StrategySearch::interpolate_missing()
{
	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].complited())
			continue;

		for (size_t j = 0; j < nr_combinations; ++j) {
			taskset ts(j+1);

			if (!ts[i])
				continue;

			interpolate_combination(i, ts);
		}
	}
}


double StrategySearch::find_max_speed() const
{
	double max_speed = 0;
	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	for (size_t j = 0; j < nr_combinations; ++j) {
		taskset ts(j+1);

		if (any_complited(ts))
			continue;

		double s = get_rate_scale(ts);
		if (s > max_speed)
			max_speed = s;
	}

	return max_speed;
}

double StrategySearch::upper_confidence_bound(taskset ts) const
{
	double m = 0;
	double s = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		m += m_estimate[i][ts.to_ulong() - 1].scale;
		s += m_estimate[i][ts.to_ulong() - 1].sigma * m_estimate[i][ts.to_ulong() - 1].sigma;
	}

	s = std::sqrt(s);

	return m + s;
}

double StrategySearch::prob_of_improvement(taskset ts) const
{
	double m = 0;
	double s = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		m += m_estimate[i][ts.to_ulong() - 1].scale;
		s += m_estimate[i][ts.to_ulong() - 1].sigma * m_estimate[i][ts.to_ulong() - 1].sigma;
	}

	if (s == 0) {
		if (m <= m_last_scale)
			return 0;
		return 1;
	}

	s = std::sqrt(s);

	double x = (m - m_last_scale - m_explore) / s;
	double pi = pnorm(x);

	if (m > m_last_scale + m_explore)
		assert(pi >= 0.5);

	return pi;
}

double StrategySearch::expected_improvement(taskset ts) const
{
	double m = 0;
	double s = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		m += m_estimate[i][ts.to_ulong() - 1].scale;
		s += m_estimate[i][ts.to_ulong() - 1].sigma * m_estimate[i][ts.to_ulong() - 1].sigma;
	}

	if (s == 0) {
		if (m <= m_last_scale)
			return 0;
		return m - m_last_scale - m_explore;
	}

	s = std::sqrt(s);

	double e = m - m_last_scale - m_explore;
	double x = e / s;
	return e * pnorm(x) + s * dnorm(x, 0, 1);
}

StrategySearch::taskset StrategySearch::find_next() const
{
	assert(!all_complited());

	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	taskset next_comb;
	double max_val = 0;

	for (size_t j = 1; j <= nr_combinations; ++j) {
		taskset ts(j);

		if (any_complited(ts))
			continue;

		double v = 0;
		if (m_acc_fun == UCB)
			v = upper_confidence_bound(ts);
		else if (m_acc_fun == PI)
			v = prob_of_improvement(ts);
		else
			v = expected_improvement(ts);

		if (v > max_val) {
			next_comb = ts;
			max_val = v;
		}
	}

	if (next_comb.count() == 0)
		next_comb = m_last_comb;

	assert(next_comb.count() > 0);

	// std::cout << next_comb << " ";
	// std::cout << max_val << " ";
	// double sc = get_rate_scale(next_comb);
	// std::cout << sc << " ";
	// double ss = find_max_speed();
	// std::cout << ss << std::endl;

	return next_comb;
}

void StrategySearch::start()
{
	init();

	collect_all_ideal();

	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	run_and_measure(nr_combinations);

	interpolate_missing();

	taskset ts_prev;
	taskset ts = find_next();

	size_t i = 0;

	while (!all_complited()) {
		i++;
		assert(!any_complited(ts));

		run_and_measure(ts);

		if (all_complited())
			break;

		if (any_complited(ts) || ts_prev != ts) {
			interpolate_missing();

			// std::stringstream output;
			// output << "table-iter-";
			// output << std::setfill('0') << std::setw(3) << i;
			// output << ".csv";
			// write_estimate(output.str());

			ts_prev = ts;
			ts = find_next();

			continue;
		}
	}
}

void StrategySearch::set_quartile(double q)
{
	m_quratile = q;
}

void StrategySearch::add_name_suffix(const std::string &s)
{
	m_name += "-";
	m_name += s;
}

bool StrategySearch::is_subset(taskset ts_min, taskset ts_max)
{
	return (ts_min & ts_max) == ts_min;
	// for (size_t i = 0; i < ts_max.size(); ++i) {
	// 	if (ts_min[i] && !ts_max[i])
	// 		return false;
	// }
    //
	// return true;
}

double StrategySearch::dnorm(double x, double a, double v)
{
	double m = (x - a) / v;
	double d = 1;
	d *= 1. / (v * std::sqrt(2. * M_PI));
	d *= std::exp(-0.5 * (m * m));
	return m;
}

double StrategySearch::pnorm(double x)
{
	double p = 0.5 * (1 + std::erf(x / std::sqrt(2)));
	return p;
}

void StrategySearch::set_explore_coeff(double e)
{
	m_explore = e;
}

