#include <cassert>
#include <iostream>

#include "StrategyFCS.hh"

StrategyFCS::StrategyFCS()
{  }

StrategyFCS::~StrategyFCS()
{  }

const char *StrategyFCS::name() const
{
	return "fcs";
}

void StrategyFCS::start()
{
	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	// std::cerr << __func__ << ":" << __LINE__ << " " <<  std::endl;

	while (!all_complited()) {
		taskset max_comb;
		double max_scale = 0;

		for (size_t j = 1; j <= nr_combinations; ++j) {
			taskset ts(j);

			if (any_complited(ts))
				continue;

			double scale = get_rate_scale(ts);
			if (scale > max_scale) {
				max_scale = scale;
				max_comb = ts;
			} else if (scale == max_scale && ts.count() > max_comb.count()) {
				max_comb = ts;
			}
		}

		assert(max_comb != 0);

		double dt = min_remaining_time(max_comb);

		// std::cerr << max_comb << " " << dt << std::endl;
		run(max_comb, dt);
	}
	// std::cerr << __func__ << ":" << __LINE__ << " " <<  std::endl;

	assert(all_complited());
}
