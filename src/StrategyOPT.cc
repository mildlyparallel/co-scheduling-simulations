#include <cassert>
#include <iostream>

#include <Problem.hh>

#include "StrategyOPT.hh"

StrategyOPT::StrategyOPT()
{  }

StrategyOPT::~StrategyOPT()
{  }

const char *StrategyOPT::name() const
{
	return "opt";
}

void StrategyOPT::start()
{
	auto distr = solve();

	for (auto [ts, dt] : distr) {
		run(ts, dt);
	}

	assert(all_complited());
}

std::vector<std::pair<Task::taskset, double>> StrategyOPT::solve()
{
	size_t nr_tasks = size();
	size_t nr_combs = (1 << size()) - 1;

	using namespace lpcpp;

	Problem lp(nr_combs);

	lp.set_verbose(Problem::Important);

	for (size_t i = 0; i < nr_tasks; ++i) {

		for (size_t j = 0; j < nr_combs; ++j)
			lp[j] = get_rate_scale(i, taskset(j+1));

		lp.add_constraint(Problem::Equal, get_work_total(i));
	}

	lp.set_bound_lower_all(0);

	lp.fill_row(1);
	lp.set_objective_minim();

	// lp.print_lp();

	lp.optimize();

	std::vector<std::pair<Task::taskset, double>> distr;

	for (size_t j = 0; j < nr_combs; j++) {
		if (lp[j] <= 1e-7)
			continue;

		taskset ts(j + 1);
		distr.push_back({ts, lp[j]});

		// std::cerr << ts << "  " << lp[j] << " ";

		// for (size_t j = 0; j < ts.size(); ++j) {
		// 	if (!ts[j])
		// 		continue;
		// 	std::cerr << get_rate_scale(i, ts) << " ";
		// }
		// std::cerr << std::endl;
	}

	return distr;
}
