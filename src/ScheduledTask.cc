#include <cassert>
#include <iostream>

#include "ScheduledTask.hh"

ScheduledTask::ScheduledTask()
{  }

ScheduledTask::~ScheduledTask()
{  }

ScheduledTask::ScheduledTask(const Task &t)
: Task(t)
{ }

bool ScheduledTask::released() const
{
	return m_released || m_release_time == 0;
}

double ScheduledTask::get_rate(taskset tasks) const
{
	return Task::get_rate(tasks, m_work_done);
}

double ScheduledTask::get_rate_scale(taskset tasks) const
{
	return Task::get_rate_scale(tasks, m_work_done);
}

void ScheduledTask::reset()
{
	m_complition_time = 0;
	m_released = false;
	m_complited = false;
	m_work_done = 0;
	m_rates_pos = 0;
}

void ScheduledTask::advance(double time_at_end, double delta_work)
{
	assert(released());
	assert(!complited());
	assert(delta_work >= 0);

	m_work_done += delta_work;

	if (m_work_done > m_work_total - WORK_EPS) {
		m_complited = true;
		m_complition_time = time_at_end;
	}
}

double ScheduledTask::run(taskset tasks, double time_now, double delta_time)
{
	assert(delta_time >= 0);

	double rate = get_rate(tasks);
	push_rate(rate);

	double dw = delta_time * rate;
	advance(time_now + delta_time, dw);

	return rate;
}

bool ScheduledTask::complited() const
{
	return m_complited;
}

void ScheduledTask::push_rate(double rate)
{
	m_rates[m_rates_pos & m_rate_history_size_mask] = rate;
	m_rates_pos++;
}

double ScheduledTask::get_rate_last() const
{
	assert(m_rates_pos > 0);

	size_t n = (m_rates_pos - 1) & m_rate_history_size_mask;
	return m_rates[n];
}

double ScheduledTask::get_rate_avg() const
{
	if (m_rates_pos == 0)
		return 0;

	double s = 0;

	if (m_rates_pos < m_rates.size()) {
		for (size_t i = 0; i < m_rates_pos; ++i)
			s += m_rates[i];
		return s / m_rates_pos;
	} 

	for (size_t i = 0; i < m_rates.size(); ++i) {
		size_t n = (m_rates_pos - i - 1) & m_rate_history_size_mask;
		s += m_rates[n];
	}

	return s / m_rates.size();
}

double ScheduledTask::get_complition_time() const
{
	return m_complition_time;
}

double ScheduledTask::get_work_done() const
{
	return m_work_done;
}
