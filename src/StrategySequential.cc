#include "StrategySequential.hh"

StrategySequential::StrategySequential()
{  }

StrategySequential::~StrategySequential()
{  }

void StrategySequential::start()
{
	while (!all_complited()) {
		size_t i = get_next_schedulable();

		taskset ts;

		if (i < size())
			ts[i] = 1;

		run(ts);
	}
}

const char *StrategySequential::name() const
{
	return "sequential";
}
