#include <cassert>

#include "System.hh"

System::System()
{  }

System::System(size_t n)
: m_tasks(n)
{
	for (size_t i = 0; i < n; ++i)
		m_tasks[i].set_id(i);
}

System::~System()
{  }

const std::vector<Task> &System::tasks() const
{
	return m_tasks;
}

void System::init_total_work(std::function<double()> fn)
{
	for (auto &t : m_tasks)
		t.set_work_total(fn());
}

void System::init_total_work(std::function<double(const Task &)> fn)
{
	for (auto &t : m_tasks)
		t.set_work_total(fn(t));
}

void System::init_total_work(double p, double dp)
{
	for (auto &t : m_tasks) {
		t.set_work_total(p);
		p += dp;
	}
}

void System::init_total_work(size_t i, double p)
{
	assert(i < m_tasks.size());
	m_tasks[i].set_work_total(p);
}

void System::init_release_time(std::function<double()> fn)
{
	for (auto &t : m_tasks) 
		t.set_release_time(fn());
}

void System::init_release_time(std::function<double(const Task &)> fn)
{
	for (auto &t : m_tasks) 
		t.set_release_time(fn(t));
}

void System::init_release_time(double r, double dr)
{
	for (auto &t : m_tasks) {
		t.set_release_time(r);
		r += dr;
	}
}

size_t System::size() const
{
	return m_tasks.size();
}

void System::set_id(unsigned id)
{
	m_id = id;
}

unsigned System::get_id() const
{
	return m_id;
}

